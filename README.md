# VNQS

## Overview
This package provides simple APIs to start backtesting using data provided by 
VNQS and the popular [backtrader](https://github.com/mementum/backtrader).

## Setup
### Install Python3.8+
Either:
- Install a version of Python that is at least 3.8 from [official page](https://www.python.org/);
- _or_ Install [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)
then [upgrade Conda to 3.8+](https://stackoverflow.com/questions/58568175/upgrade-to-python-3-8-using-conda).

### Install Poetry (Optional)
[Poetry](https://python-poetry.org/) is the framework that helps us maintain our code with credible, 
updated and well-linked dependencies. This also helps you maintain your model with
your desired version of VNQS.

To install Poetry, simply follow the provided [instructions](https://python-poetry.org/docs/#installation) 
for your own platform.


### Download data
The provided data currently only extend to __daily__ and __minutely__ bars for all 
stocks. These are __adjusted prices__ with __survivorship bias__. 

Download the data by following the steps:

0. Pre-requisites
    ```
    git
    python3.8+
    ```
1. Activate your Conda (or any desired virtual environment manager) environment
2. Run
    ```
    pip install git+https://bitbucket.org/vnquantsmith/vnqs/src/master/
    ```

    You should see some output such as

    ```
    (base)
    # ngtrunghuan @ DESKTOP-N939V4V in ~/qs [1:59:24] C:1
    $ pip install ./vnqs
    Processing ./vnqs
    Installing build dependencies ... done
    Getting requirements to build wheel ... done
        Preparing wheel metadata ... done
    [...]
    Successfully built vnqs
    Installing collected packages: vnqs
    Successfully installed vnqs-1.0.0-1
    ```

3. Run the download script. This will take anywhere between 10-20 minutes.

    ```
    (base)
    # ngtrunghuan @ DESKTOP-N939V4V in ~/qs [1:59:29] 
    $ download_data
    2021.01.02T02.01.49,324 [vnqs.researchbase.data.download::cache_minutely_candles::40] INFO: Downloading minutely candles...
    2021.01.02T02.01.50,011 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 0 files.
    2021.01.02T02.01.50,012 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 100 files.
    2021.01.02T02.01.50,014 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 200 files.
    2021.01.02T02.01.50,016 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 300 files.
    2021.01.02T02.01.50,017 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 400 files.
    2021.01.02T02.01.50,018 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 500 files.
    2021.01.02T02.01.50,019 [vnqs.researchbase.data.download::download_all_files_in_bucket::28] INFO: Downloaded 600 files.
    ```

### Start your research
1. Check if the package is successfully installed by running Python on a terminal or in a Jupyter notebook

    ```
    import vnqs
    print(vnqs.__version__)
    ```

2. We recommend you to use a Jupyter notebook to start your research work. A sample code is provided [here](./vnqs/researchbase/sample_strat.py), but you should aspire to a more [complicated setup](https://www.backtrader.com/home/references/blogs/) as seen in the backtrader community. Please contribute back to this repository via a [pull request](https://bitbucket.org/vnquantsmith/vnqs/pull-requests/new)

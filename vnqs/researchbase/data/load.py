import logging
from typing import Generator, Optional
from pathlib import Path

import pandas as pd

from .constants import CACHE_DIRECTORY, DAILY, MINUTELY

logger = logging.getLogger(__name__)


def read_candles(files: Generator[Path, None, None]) -> Optional[pd.DataFrame]:
    try:
        return (
            pd.concat([pd.read_csv(f, parse_dates=["reference_period"]) for f in files])
            .sort_values(["reference_period"])
            .rename(columns={"reference_period": "datetime"})
            .drop(columns=["symbol"], errors="ignore")
            .set_axis(["datetime", "open", "high", "low", "close", "volume"], axis=1)
            .set_index("datetime")
        )
    except ValueError as err:
        logger.error(
            f"{repr(err)}. No candles found. Have you cached the data?"
        )
        return None


def get_daily_candles(symbol: str) -> Optional[pd.DataFrame]:
    files = (Path(CACHE_DIRECTORY) / DAILY).glob(f"*/{symbol}.csv")
    logger.info(f"Loading daily candles for {symbol}.")
    return read_candles(files)


def get_minutely_candles(symbol: str) -> Optional[pd.DataFrame]:
    files = (Path(CACHE_DIRECTORY) / MINUTELY).glob(f"**/{symbol}.csv")
    logger.info(f"Loading minutely candles for {symbol}.")
    return read_candles(files)
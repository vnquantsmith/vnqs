import os
from pathlib import Path

CACHE_DIRECTORY = Path(os.environ.get("VNQS_CACHE", Path.home() / ".vnqs.cache"))
DAILY = "de-ssi-daily-bars"
MINUTELY = "de-vietstock-minutely-bars"
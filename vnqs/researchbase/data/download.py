import logging
import asyncio
from pathlib import Path
from multiprocessing import Pool

from google.cloud import storage

from vnqs.logger import setup_root_logger_to_stdout
from .constants import CACHE_DIRECTORY, DAILY, MINUTELY


logger = logging.getLogger(__name__)

async def download_file(cache_dir, bucket_name, blob):
    destination_file = Path(cache_dir) / bucket_name / blob.name
    if destination_file.is_file():
        return
    else:
        destination_file.parent.mkdir(parents=True, exist_ok=True)
        blob.download_to_filename(destination_file.as_posix())
        return

async def download_all_files_in_bucket(cache_dir: str, bucket_name: str):
    storage_client = storage.Client.create_anonymous_client()
    blobs = storage_client.list_blobs(bucket_name)
    for idx, blob in enumerate(blobs):
        if idx % 100 == 0:
            logger.info(f"Downloaded {idx} files.")
        await download_file(cache_dir, bucket_name, blob)
    logger.info(f"Finished caching files from {bucket_name} into {cache_dir}.")


def cache_daily_candles():
    logger.info("Downloading daily candles...")
    asyncio.run(download_all_files_in_bucket(CACHE_DIRECTORY, DAILY))
    logger.info("Downloaded all daily candles.")


def cache_minutely_candles():
    logger.info("Downloading minutely candles...")
    asyncio.run(download_all_files_in_bucket(CACHE_DIRECTORY, MINUTELY))
    logger.info("Downloaded all minutely candles.")


def main():
    setup_root_logger_to_stdout(logging.INFO)
    cache_daily_candles()
    # cache_minutely_candles()

if __name__ == "__main__":
    main()
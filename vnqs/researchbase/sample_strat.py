import logging

import backtrader as bt

from vnqs.researchbase.data.load import get_daily_candles, get_minutely_candles
from vnqs.logger import setup_root_logger_to_stdout

logger = logging.getLogger(__name__)


def run_strat():
    cerebro = bt.Cerebro(stdstats=False)
    cerebro.addstrategy(bt.Strategy)
    symbol = "FPT"
    logging.info(f"Loading data for {symbol}...")
    data_df = get_minutely_candles(symbol)
    logger.info(
        f"Loaded data for {symbol}; len={len(data_df)}; "
        f"range={data_df.index.min()}, {data_df.index.max()}"
    )
    data = bt.feeds.PandasData(dataname=data_df)
    cerebro.adddata(data)

    # Run over everything
    cerebro.run()

    # Plot the result
    cerebro.plot(style='line')



if __name__ == "__main__":
    setup_root_logger_to_stdout(logging.INFO)
    run_strat()
import logging
from typing import Optional
from datetime import datetime

LOG_FORMAT = (
    "%(asctime)s [%(name)s::%(funcName)s::%(lineno)d] %(levelname)s: %(message)s"
)


class MillisecondLogFormatter(logging.Formatter):
    converter = datetime.fromtimestamp

    def formatTime(self, record: logging.LogRecord, datefmt: Optional[str]) -> str:
        ct = self.converter(record.created)
        s = ct.strftime(datefmt)
        s = f"{s}."
        return s


def setup_root_logger_to_stdout(level):
    formatter = logging.Formatter(LOG_FORMAT)
    formatter.default_time_format = "%Y.%m.%dT%H.%M.%S"
    # logging.setFormatter(formatter)
    logging.basicConfig(level=level)
    root_logger = logging.getLogger()
    for handler in root_logger.handlers:
        handler.setFormatter(formatter)
    root_logger.propagate = True

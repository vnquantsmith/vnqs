import logging
import logging
import os
from pathlib import Path
from typing import Dict

import pandas as pd
from trading_calendars import register_calendar
from zipline.assets.asset_writer import AssetDBWriter
from zipline.data.adjustments import SQLiteAdjustmentWriter
from zipline.data.bcolz_daily_bars import BcolzDailyBarWriter
from zipline.data.bundles import register
from zipline.data.minute_bars import BcolzMinuteBarWriter
from zipline.utils.calendars import TradingCalendar
from zipline.utils.cache import dataframe_cache

from vnqs.logger import setup_root_logger_to_stdout
from vnqs.researchbase.hose_calendar import HOSECalendar

logger = logging.getLogger("extension")

setup_root_logger_to_stdout(logging.INFO)

# TODO: load and cache data from GCP into HOSE_DAILY_DATA_PATH.

# Register calendar
start_session = pd.Timestamp("2000-01-03", tz="utc")
end_session = pd.Timestamp("2020-12-31", tz="utc")

hose_cal = HOSECalendar()
register_calendar("HOSE", hose_cal)

# Register data based on the path provided in .env

# register(
#     "hose-daily",
#     csvdir_equities(
#         ["daily"],
#         os.environ["HOSE_DAILY_DATA_PATH"],
#     ),
#     calendar_name="HOSE",
#     start_session=start_session,
#     end_session=end_session,
# )


def gen_asset_metadata(data, show_progress):
    if show_progress:
        logger.info("Generating asset metadata.")

    data = data.groupby(by="symbol").agg({"date": ["min", "max"]})
    data.reset_index(inplace=True)
    data["start_date"] = data.date.amin
    data["end_date"] = data.date.amax
    del data["date"]
    data.columns = data.columns.get_level_values(0)

    data["exchange"] = "HOSE"
    data["auto_close_date"] = data["end_date"].values + pd.Timedelta(days=1)
    return data


@register("vn")
def vn_bundle(
    environ: Dict,
    asset_db_writer: AssetDBWriter,
    minute_bar_writer: BcolzMinuteBarWriter,
    daily_bar_writer: BcolzDailyBarWriter,
    adjustment_writer: SQLiteAdjustmentWriter,
    calendar: TradingCalendar,
    start_session: pd.Timestamp,
    end_session: pd.Timestamp,
    cache: dataframe_cache,
    show_progress: bool,
    output_dir: str,
):
    raw_data = pd.DataFrame()
    asset_metadata = gen_asset_metadata(raw_data[["symbol", "date"]], show_progress)
    asset_db_writer.write(asset_metadata)

    symbol_map = asset_metadata.symbol
    sessions = calendar.sessions_in_range(start_session, end_session)

    raw_data.set_index(["date", "symbol"], inplace=True)
    daily_bar_writer.write(raw_data, show_progress=show_progress)

    # raw_data.reset_index(inplace=True)
    # raw_data['symbol'] = raw_data['symbol'].astype('category')
    # raw_data['sid'] = raw_data.symbol.cat.codes
    # adjustment_writer.write(
    #     splits=parse_splits(
    #         raw_data[[
    #             'sid',
    #             'date',
    #             'split_ratio',
    #         ]].loc[raw_data.split_ratio != 1],
    #         show_progress=show_progress
    #     ),
    #     dividends=parse_dividends(
    #         raw_data[[
    #             'sid',
    #             'date',
    #             'ex_dividend',
    #         ]].loc[raw_data.ex_dividend != 0],
    #         show_progress=show_progress
    #     )
    # )


logger.info('Registered calendar "HOSE" amd bundle "hose-daily".')
